import DS from 'ember-data';

export default DS.JSONSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let newPayload = new Array();
    newPayload.pushObject(payload);
    newPayload.forEach(user => {
      user.avatarUrl = user.avatar_url;
      delete user.avatar_url;

      user.webUrl = user.web_url;
      delete user.web_url;
    })
    return this._super(store, primaryModelClass, newPayload, id, requestType);
  }
});
