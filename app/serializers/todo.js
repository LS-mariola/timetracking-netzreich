import DS from 'ember-data';

export default DS.JSONSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload.forEach(element => {
      element.title = element.target.title;
      element.actionName = element.action_name;
      element.projectName = element.project.name_with_namespace;
      element.projectId = element.project.id;
      element.issueId = element.target.iid;
      element.timeEstimate = element.target.time_stats.time_estimate;
      element.humanTimeEstimate = element.target.time_stats.human_time_estimate;
      element.totalTimeSpent = element.target.time_stats.total_time_spent;
      element.humanTotalTimeSpent = element.target.time_stats.human_total_time_spent;
    });
    return this._super(store, primaryModelClass, payload, id, requestType);
  }

});
