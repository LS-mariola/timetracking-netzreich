import DS from 'ember-data';

export default DS.Model.extend({
  // Attributes
  name: DS.attr('string'),
  avatarUrl: DS.attr('string'),
  webUrl: DS.attr('string')
});
