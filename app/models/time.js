import DS from 'ember-data';

export default DS.Model.extend({
  date: DS.attr('number'),
  timerInSeconds: DS.attr('number'),
  message: DS.attr('string'),

  todoId: DS.attr('number'),
  todoName: DS.attr('string'),
  
  userId: DS.attr('number'),
  projectId: DS.attr('number'),
  issueId: DS.attr('number')
});
