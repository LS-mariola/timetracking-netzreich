import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  actionName: DS.attr('string'),
  humanTimeEstimate: DS.attr('string'),
  timeEstimate: DS.attr('number'),
  humanTotalTimeSpent: DS.attr('string'),
  totalTimeSpent: DS.attr('number'),

  projectId: DS.attr('number'),
  projectName: DS.attr('string'),
  
  issueId: DS.attr('number'),

  postSaveTimeSpendOnGitLab(params) {
    let adapter = this.get('store').adapterFor(this.constructor.modelName);
    return adapter.postRequest(this, params);
  }
});
