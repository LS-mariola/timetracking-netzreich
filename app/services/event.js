import Service from '@ember/service';
import Evented from '@ember/object/evented';
import Ember from 'ember';

const {
  get
} = Ember;

export default Service.extend(Evented, {
  menuBool: true,

  toggle() {
    this.toggleProperty('menuBool');
  },

  showToggle() {
    return get(this, 'menuBool');
  }
});
