import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTAdapter.extend({
  session: Ember.inject.service('session'),
  host: 'https://projekte.netzreich.de',
  namespace: 'api/v4',
  
  headers: Ember.computed('session', function() {
    return {
      // 'Authorization': 'Bearer ' + this.get('session.data.authenticated.authorizationToken.access_token')
      'Authorization': this.get('session.data.authenticated.authorizationToken.token_type') + ' '+ this.get('session.data.authenticated.authorizationToken.access_token')
    };
  })
});
