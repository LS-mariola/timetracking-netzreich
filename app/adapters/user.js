import AppAdapter from './application';

export default AppAdapter.extend({
    pathForType(modelName) {
        return modelName;
    }
});
