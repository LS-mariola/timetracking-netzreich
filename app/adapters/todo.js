import AppAdapter from './application';
import Ember from 'ember';
const { get } = Ember;

export default AppAdapter.extend({
    postRequest(model, params) {
        let projectId = get(model, 'projectId');
        let issueId = get(model, 'issueId');
        
        let url = `${this.host}/${this.namespace}/projects/${projectId}/issues/${issueId}/add_spent_time`
        return this.ajax(url, 'POST', {data: params});
    }
});
