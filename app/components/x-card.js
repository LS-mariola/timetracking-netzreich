import Component from '@ember/component';
import Ember from 'ember';
import {
  isArray
} from '@ember/array';

const {
  get,
  set
} = Ember;

export default Component.extend({
  classNames: ['sidebar', 'sidebar-cards', 'col-12'],

  store: Ember.inject.service(),
  event: Ember.inject.service('event'),
  userId: null,
  todo: null,

  modelTime: null,
  collectionTime: null,

  didReceiveAttrs() {
    this._collectTimetrackingData();
  },

  actions: {
    editCard(card) {
      card.set('isEditing', true);
    },
    saveCard(card) {
      card.set('isEditing', false);
      let hoursInput = document.querySelector('#hours');
      let minutesInput = document.querySelector('#minutes');
      let secondsInput = document.querySelector('#seconds');

      let oldTimerInSeconds = get(card, 'timerInSeconds');
      card.timerInSeconds = this._convertToSeconds(hoursInput.value, minutesInput.value, secondsInput.value);
      card.save();

      let diffTimerInSeconds = card.timerInSeconds - oldTimerInSeconds;
      this._additionTimeSpend(diffTimerInSeconds);

      get(this, 'event').trigger('postTimeSpend', get(this, 'todo'), diffTimerInSeconds);
    },
    cancelCardEditing(card) {
      card.set('isEditing', false);
      card.rollbackAttributes();
    },
    deleteCard(card) {
      let confirmation = confirm('Are you sure?');
      if (confirmation) {
        let diffTimerInSeconds = get(card, 'timerInSeconds') - 2 * get(card, 'timerInSeconds');
        console.log('negativniBroj:', -Math.abs(get(card, 'timerInSeconds')))
        card.destroyRecord();
        this._updateLoadedCollection(card);

        this._additionTimeSpend(diffTimerInSeconds);

        get(this, 'event').trigger('postTimeSpend', get(this, 'todo'), diffTimerInSeconds);
      }
    }
  },

  _collectTimetrackingData() {
    this.get('store').query('time', {
      orderBy: 'projectId',
      equalTo: get(this, 'todo.data.projectId')
    }).then(element => {
      let tempFilter = element.filterBy('data.issueId', get(this, 'todo.data.issueId'));
      let tempFilter2 = tempFilter.filterBy('data.userId', parseInt(get(this, 'userId')));
      set(this, 'modelTime', tempFilter2.sortBy('data.date').reverse());

      this._additionTimeSpend(tempFilter2);
    });
  },

  _updateLoadedCollection(card) {
    let oldData = get(this, 'modelTime');
    set(this, 'modelTime', oldData.removeObject(card));
  },

  _additionTimeSpend(array) {
    if (isArray(array)) {
      let sum = 0;
      array.forEach(item => {
        sum += item.data.timerInSeconds;
      })

      set(this, 'collectionTime', sum);
    } else {
      let oldTimeSpend = get(this, 'collectionTime');
      oldTimeSpend += array;
      set(this, 'collectionTime', oldTimeSpend);
    }
  },

  _convertToSeconds(hours, minutes, seconds) {
    return parseInt(hours) * 3600 + parseInt(minutes) * 60 + parseInt(seconds);
  }
});
