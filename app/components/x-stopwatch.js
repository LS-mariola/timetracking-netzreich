import Component from '@ember/component';
import Ember from 'ember';
import moment from 'moment';
import {
  task,
  timeout
} from 'ember-concurrency';

const {
  get,
  set
} = Ember;

export default Component.extend({
  isClockOnNull: true,
  result: '00:00:00',
  timer: 0,
  percentValue: 0,
  IsDisabledStop: true,
  disableStopButton: 'display:none;',

  startTimer: task(function* () {
    while (get(this, 'isEnableResult')) {
      yield timeout(1000);
      this.incrementProperty('timer');
      this._formatSecond(this.get('timer'));
      this._setPercentValue(this.get('timer'));
    }
  }).restartable(),

  actions: {
    start() {
      this.sendAction('trackingIsOn', true);
      set(this, 'isEnableResult', true);
      set(this, 'disableStopButton', '');
      this._formatSecond(get(this, 'timer'));
      get(this, 'startTimer').perform();
    },

    resume() {
      get(this, 'startTimer').perform();
    },

    pause() {
      get(this, 'startTimer').cancelAll();
      set(this, 'isClockOnNull', false);
    },

    stop() {
      get(this, 'startTimer').cancelAll();
      let timerInSeconds = get(this, 'timer');
      set(this, 'timer', 0);
      set(this, 'isClockOnNull', true);
      set(this, 'disableStopButton', 'display:none;');
      this.sendAction('trackingIsOn', false, get(this, 'result'), timerInSeconds);
    }
  },

  _formatSecond(second) {
    let newFormat = moment.utc(second * 1000).format('HH:mm:ss');
    set(this, 'result', newFormat);
  },
  _setPercentValue(second) {
    while (second > 59) {
      second = second - 60;
    }
    let percent = second * 1.67;
    set(this, 'percentValue', percent);
  }
});
