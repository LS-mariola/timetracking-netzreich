import Chart from 'ember-easy-pie-chart/components/easy-pie-chart'

export default Chart.extend({
    // disable text and sign % from easy-pie-chart addon
    percentText: null,
    percentSign: null
});
