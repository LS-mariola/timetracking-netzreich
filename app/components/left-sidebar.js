import Component from '@ember/component';
import Ember from 'ember';
const {
  get
} = Ember;

export default Component.extend({
  classNames: ['scroll-wrapper', 'scrollbar-inner'],

  trackingIn: false,

  actions: {
    selectItem(issue) {
      if (!(get(this, 'trackingIn'))) {
        this.sendAction('selectItem', issue);
      }
    }
  }
});
