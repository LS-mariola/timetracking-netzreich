import Component from '@ember/component';
import Ember from 'ember';
const {
  get
} = Ember;

export default Component.extend({
  tagName: 'header',

  classNames: ['header'],

  session: Ember.inject.service('session'),
  event: Ember.inject.service('event'),

  actions: {
    LogOut() {
      get(this, 'session').invalidate();
      localStorage.clear();
    },
    showMenu() {
      get(this, 'event').toggle();
      this.sendAction('showMenu', get(this, 'event').showToggle());
    }
  }
});
