import Component from '@ember/component';
import Ember from 'ember';
const {
  get,
  set
} = Ember;

export default Component.extend({
  classNames: ['col-12', 'content-tracking'],

  isEnabledMessage: false,
  clock: '',
  timerInSeconds: null,

  didUpdateAttrs() {
    this._super(...arguments);
    set(this, 'percentValue', 0);
  },

  actions: {
    trackingIsOn(boolValue, timeTracking, timerInSeconds) {
      set(this, 'isEnabledMessage', !(boolValue));

      if (boolValue) {
        this.sendAction('trackingIsOn', boolValue);
      } else {
        set(this, 'clock', timeTracking);
        set(this, 'timerInSeconds', timerInSeconds);
      }
    },

    saveMessage() {
      this.sendAction('saveTracking', get(this, 'message'), get(this, 'timerInSeconds'));
      this.send('cancelTracking');
    },

    cancelTracking() {
      this.sendAction('trackingIsOn', false);
    }
  }
});
