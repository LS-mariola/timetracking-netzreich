import Oauth2 from 'torii/providers/oauth2-bearer';

export default Oauth2.extend({
  name:       'gitlab-oauth2',
  baseUrl:    'https://projekte.netzreich.de/oauth/authorize',

  requiredUrlParams: ['client_id', 'redirect_uri', 'response_type', 'state'],

  responseParams: ['access_token', 'state', 'token_type']
});