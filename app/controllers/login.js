import Controller from '@ember/controller';
import Ember from 'ember';
const { get } = Ember;

export default Controller.extend({
    session: Ember.inject.service('session'),

    actions: {
        authenticate() {
            get(this, 'session').authenticate('authenticator:torii', 'gitlab-oauth2');
        }
    }
});
