import Controller from '@ember/controller';
import Ember from 'ember';
import moment from 'moment';
const {
  get,
  set
} = Ember;

export default Controller.extend({
  event: Ember.inject.service('event'),

  trackingIsOn: false,
  isEnable: false,
  todo: null,
  resetValue: false,

  init() {
    get(this, 'event').on('postTimeSpend', (modelObject, timerInSeconds) => {
      this._postTimeSpendOnGitLab(modelObject, timerInSeconds)
    });
    this._super(...arguments);
  },

  actions: {
    selectTodoIssue(issue) {
      set(this, 'todo', issue);

      set(this, 'isEnable', true);
      set(this, 'resetValue', false);

      get(this, 'event').toggle();
      this.send('showMenu', get(this, 'event').showToggle());
    },

    trackingIsOn(boolValue) {
      set(this, 'trackingIsOn', boolValue);
      set(this, 'isEnable', boolValue);
    },

    saveTracking(message, timerInSeconds) {
      set(this, 'isEnable', false);
      this._saveRecord(message, timerInSeconds);
    },

    showMenu(bool) {
      if (bool) {
        set(this, 'classNameMenu', '');
      } else {
        set(this, 'classNameMenu', 'collapse');
      }
    }
  },

  _saveRecord(message, timerInSeconds) {
    let userFromData = get(this, 'store').peekAll('user');

    this.store.createRecord('time', {
      date: moment().unix(),
      todoId: get(this, 'todo.id'),
      todoName: get(this, 'todo.data.title'),
      timerInSeconds: timerInSeconds,
      message: message,
      userId: userFromData.get('firstObject').id,
      issueId: get(this, 'todo.data.issueId'),
      projectId: get(this, 'todo.data.projectId')
    }).save();

    this._postTimeSpendOnGitLab(get(this, 'todo'), timerInSeconds);
  },

  _postTimeSpendOnGitLab(modelObject, timerInSeconds) {
    let value = this._convertTime(timerInSeconds);
    modelObject.postSaveTimeSpendOnGitLab({
      duration: value
    });
  },

  _convertTime(estimateSeconds) {
    let outputFormatingString = null;
    let minus = '';
    estimateSeconds = parseInt(estimateSeconds);

    let weeks = 0;
    let days = 0;
    let hours = 0;
    let minutes = 0;
    let seconds = 0;
    if (estimateSeconds > 0) {
      while (estimateSeconds != 0) {
        if (estimateSeconds >= 144000) {
          estimateSeconds -= 144000;
          weeks = weeks + 1;
        } else if (estimateSeconds >= 28800) {
          estimateSeconds -= 28800;
          days = days + 1;
        } else if (estimateSeconds >= 3600) {
          estimateSeconds -= 3600;
          hours = hours + 1;
        } else if (estimateSeconds >= 60) {
          estimateSeconds -= 60;
          minutes = minutes + 1;
        } else if (estimateSeconds > 0) {
          seconds = estimateSeconds;
          estimateSeconds -= seconds;
        }
      }
    } else {
      // estimateSeconds < 0
      minus = '-';
      while (estimateSeconds != 0) {
        if (estimateSeconds <= -144000) {
          estimateSeconds += 144000;
          weeks = weeks + 1;
        } else if (estimateSeconds <= -28800) {
          estimateSeconds += 28800;
          days = days + 1;
        } else if (estimateSeconds <= -3600) {
          estimateSeconds += 3600;
          hours = hours + 1;
        } else if (estimateSeconds <= -60) {
          estimateSeconds += 60;
          minutes = minutes + 1;
        } else if (estimateSeconds < 0) {
          seconds = estimateSeconds * (-1);
          estimateSeconds += seconds;
        }
      }
    }

    if (weeks > 0) {
      outputFormatingString = minus + weeks + 'w';
    }
    if (days > 0) {
      if (outputFormatingString != null) {
        outputFormatingString = outputFormatingString + ' ' + days + 'd';
      } else {
        outputFormatingString = minus + days + 'd';
      }
    }
    if (hours > 0) {
      if (outputFormatingString != null) {
        outputFormatingString = outputFormatingString + ' ' + hours + 'h';
      } else {
        outputFormatingString = minus + hours + 'h';
      }
    }
    if (minutes > 0) {
      if (outputFormatingString != null) {
        outputFormatingString = outputFormatingString + ' ' + minutes + 'm';
      } else {
        outputFormatingString = minus + minutes + 'm';
      }
    }
    if (seconds > 0) {
      if (outputFormatingString != null) {
        outputFormatingString = outputFormatingString + ' ' + seconds + 's';
      } else {
        outputFormatingString = minus + seconds + 's';
      }
    }
    return outputFormatingString;
  }
});
