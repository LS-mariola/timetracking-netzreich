import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import Ember from 'ember';
const {
  get
} = Ember;

export default Route.extend(ApplicationRouteMixin, {
  routeAfterAuthentication: 'timetracking',
  session: Ember.inject.service('session'),

  authenticationSucceeded() {
    this.transitionTo('timetracking');
  },

  sessionInvalidated() {
    this.transitionTo('login');
  },

  redirect() {
    let session = get(this, 'session');
    if (session.isAuthenticated) {
      this.transitionTo('timetracking');
    }
  }
});
