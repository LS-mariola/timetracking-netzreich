import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Ember from 'ember';
const {
  get
} = Ember;

export default Route.extend(AuthenticatedRouteMixin, {

  model() {
    return Ember.RSVP.hash({
      todo: get(this, 'store').findAll('todo'),
      user: get(this, 'store').findAll('user')
    });
  }
});
