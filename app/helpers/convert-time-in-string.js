import {
  helper
} from '@ember/component/helper';

export function convertTimeInString(params /*, hash*/ ) {
  let estimateSeconds = params[0];
  let outputFormatingString = null;
  let minus = '';
  estimateSeconds = parseInt(estimateSeconds);

  let weeks = 0;
  let days = 0;
  let hours = 0;
  let minutes = 0;
  let seconds = 0;
  if (estimateSeconds > 0) {
    while (estimateSeconds != 0) {
      if (estimateSeconds >= 144000) {
        estimateSeconds -= 144000;
        weeks = weeks + 1;
      } else if (estimateSeconds >= 28800) {
        estimateSeconds -= 28800;
        days = days + 1;
      } else if (estimateSeconds >= 3600) {
        estimateSeconds -= 3600;
        hours = hours + 1;
      } else if (estimateSeconds >= 60) {
        estimateSeconds -= 60;
        minutes = minutes + 1;
      } else if (estimateSeconds > 0) {
        seconds = estimateSeconds;
        estimateSeconds -= seconds;
      }
    }

  }

  if (weeks > 0) {
    outputFormatingString = minus + weeks + 'w';
  }
  if (days > 0) {
    if (outputFormatingString != null) {
      outputFormatingString = outputFormatingString + ' ' + days + 'd';
    } else {
      outputFormatingString = minus + days + 'd';
    }
  }
  if (hours > 0) {
    if (outputFormatingString != null) {
      outputFormatingString = outputFormatingString + ' ' + hours + 'h';
    } else {
      outputFormatingString = minus + hours + 'h';
    }
  }
  if (minutes > 0) {
    if (outputFormatingString != null) {
      outputFormatingString = outputFormatingString + ' ' + minutes + 'm';
    } else {
      outputFormatingString = minus + minutes + 'm';
    }
  }
  if (seconds > 0) {
    if (outputFormatingString != null) {
      outputFormatingString = outputFormatingString + ' ' + seconds + 's';
    } else {
      outputFormatingString = minus + seconds + 's';
    }
  }
  return outputFormatingString;
}

export default helper(convertTimeInString);
