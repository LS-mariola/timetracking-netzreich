import {
  helper
} from '@ember/component/helper';

export function convertSeconds(params /*, hash*/ ) {
  let inputValue = params[0];
  
  let hours = parseInt(inputValue / 3600);
  let rest = inputValue - hours * 3600;
  let minutes = parseInt(rest / 60);
  let seconds = rest - minutes * 60;

  if (params[1] == 'h') {
    return hours;
  } else if (params[1] == 'm') {
    return minutes;
  } else if (params[1] == 's') {
    return seconds;
  } else {
    return 0
  }
}

export default helper(convertSeconds);
